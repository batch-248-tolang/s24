console.log("Let's get started");


// Exponent Operator
let base = 7
let exponent = 3

const getCube = base ** exponent;

console.log(`The cube of ${base} is ${getCube}`);


// Array Destructuring
let address = ["69","Curve Ave","SW","Cebu","69420"]

let [houseNumber, street, direction, city, zipCode] = address;

console.log(`I live at ${houseNumber} ${street} ${direction}, ${city} ${zipCode}`);


// Object Destructuring
let animal = {
	animalName:"Lolong",
	kind:"Crocodile",
	type:"Saltwater",
	weight:"1075 kgs",
	size:"20 ft 3 in",

}

let {animalName, kind, type, weight, size} = animal;

console.log(`${animalName} was a ${type} ${kind}. He weighed at ${weight} with a measurement of ${size}.`);


// Arrow Functions
 const numbers = [1,2,3,4,5];

 numbers.forEach((number)=>{
 	console.log(number);
 });

 const initialValue = 0;
 const totalVal = numbers.reduce(
   (accumulator, currentValue) => accumulator + currentValue,
   initialValue
 );

 console.log(totalVal);


 // Javascript Classes
 class Dog {
 	constructor(name,age,breed){
 		this.name = name;
 		this.age = age;
 		this.breed = breed;

 	}
 }

 const myDog = new Dog("Maui","7","Japanese Spitz");
 console.log(myDog);